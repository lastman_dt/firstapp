//
//  DTAlerts.m
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 21/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "DTAlertAudioError.h"

@implementation DTAlertAudioError

+ (void)presentPlayingAudioErrorAlertFromViewController:(UIViewController *)viewController {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"playingAudioErrorAlert.title", nil)
                                                                   message:NSLocalizedString(@"playingAudioErrorAlert.message", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action) {}];
    [alert addAction:okButton];
    [viewController presentViewController:alert animated:YES completion:nil];
}

@end
