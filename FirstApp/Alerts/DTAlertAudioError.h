//
//  DTAlerts.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 21/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DTAlertAudioError : NSObject

+ (void)presentPlayingAudioErrorAlertFromViewController:(UIViewController *)viewController;

@end

NS_ASSUME_NONNULL_END
