//
//  DTPresentViewController.m
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 07/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "DTPresetViewController.h"
#import <Realm/Realm.h>
#import "DTPreset.h"
#import <AVFoundation/AVFoundation.h>
#import "DTPresetTableViewCell.h"
#import "UIColor+DTColor.h"
#import "DTDetailViewController.h"
#import "DTAlertAudioError.h"
#import "AFURLSessionManager.h"

NSString * const kDTPresetViewControllerCellReuseIdentifier = @"kVKPresetTableViewCellReuseIdentifier";
NSString * const kDTPresetViewControllerPresetsConfigURL = @"https://dl.dropboxusercontent.com/s/fyxor3v7gz3pjiz/presetConfig.txt?dl=0";
CGFloat    const kDTPresetViewControllerTableViewRowHeight = 100.f;

@interface DTPresetViewController () <UITableViewDataSource, DTPresetTableViewCellDelegate, AVAudioPlayerDelegate>

@property (nonatomic, strong) RLMResults *presets;
@property (nonatomic, strong) AVAudioPlayer *player;
@property (nonatomic, strong) NSIndexPath *playingCellIndexPath;
@property (nonatomic, strong) NSTimer *progressTimer;
@property (nonatomic, assign) CGFloat progress;

@end

@implementation DTPresetViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];

    if ([DTPreset allObjects].count == 0) {
        [self downloadPresetsConfigFromLocalConfig];
    }
    [self downloadPresetsConfigWithCompletion:nil];
    self.presets = [[DTPreset allObjects] sortedResultsUsingKeyPath:@"orderBy" ascending:YES];
    [self.tableView registerClass:[DTPresetTableViewCell class] forCellReuseIdentifier:kDTPresetViewControllerCellReuseIdentifier];
    [self configureViewController];
    [self configureTableView];
    [self addRefreshControl];
}

#pragma mark - Custom Accessors

- (void)setProgress:(CGFloat)progress {
    _progress = progress;
    DTPresetTableViewCell *cell = [self.tableView cellForRowAtIndexPath:self.playingCellIndexPath];
    cell.progressTimePoint = _progress;
}

#pragma mark - Action

- (void)refreshTable:(UIRefreshControl *)refreshControl {
    [refreshControl beginRefreshing];
    [self stopPlayingAudio];
    [self downloadPresetsConfigWithCompletion:^{
        [self resetPlayingCell];
        [self downloadPresetsConfigFromLocalConfig];
        [self.tableView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [refreshControl endRefreshing];
        });
    }];
}

#pragma mark - Private

- (void)configureViewController {
    self.title = @"Road to iOS Rockstar";
    self.navigationController.navigationBar.barTintColor = [UIColor dt_darkDesaturatedBlueColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont systemFontOfSize:12.f]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)configureTableView {
    self.tableView.backgroundColor = [UIColor dt_veryDarkBlueColor];
    self.tableView.rowHeight = kDTPresetViewControllerTableViewRowHeight;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)addRefreshControl {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:refreshControl];
}

- (void)downloadPresetsConfigFromLocalConfig {
    NSArray *presetConfigDictionaries = [self presetConfigDictionariesFromFile:@"presetConfig.txt"];
    NSMutableArray *presetsToAddOrUpdate = [[NSMutableArray alloc] init];
    NSMutableArray *allPresetsFromConfig = [[NSMutableArray alloc] init];
    for (NSDictionary *presetConfigDictionary in presetConfigDictionaries) {
        DTPreset *presetFromConfig = [DTPreset presetFromConfigDictionary:presetConfigDictionary];
        DTPreset *existedPreset = [DTPreset objectForPrimaryKey:@(presetFromConfig.idItem)];
        if (!existedPreset) {
            [presetsToAddOrUpdate addObject:presetFromConfig];
        } else if (![presetFromConfig isEqualToPreset:existedPreset]) {
            [presetsToAddOrUpdate addObject:presetFromConfig];
        }
        [allPresetsFromConfig addObject:presetFromConfig];
    }
    
    NSMutableArray *presetsForRemove = [[NSMutableArray alloc] init];
    for (DTPreset *preset in [DTPreset allObjects]) {
        [presetsForRemove addObject:preset];
    }
    
    [presetsForRemove removeObjectsInArray:allPresetsFromConfig];
    
    RLMRealm *realm = RLMRealm.defaultRealm;
    [realm beginWriteTransaction];
    [realm addOrUpdateObjects:presetsToAddOrUpdate];
    [realm deleteObjects:presetsForRemove];
    [realm commitWriteTransaction];
}

- (void)downloadPresetsConfigWithCompletion:(void(^)(void))completion {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *URL = [NSURL URLWithString:kDTPresetViewControllerPresetsConfigURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request
                                                                     progress:nil
                                                                  destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                                                                      NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory
                                                                                                                                            inDomain:NSUserDomainMask
                                                                                                                                   appropriateForURL:nil
                                                                                                                                              create:NO
                                                                                                                                               error:nil];
                                                                      
                                                                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                                                      if ([httpResponse statusCode] == 200) {
                                                                          
                                                                          NSURL *fileURL = [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
                                                                          
                                                                          NSError *error = nil;
                                                                          if ([[NSFileManager defaultManager] isDeletableFileAtPath:fileURL.path]) {
                                                                              BOOL isFileDeleted = [[NSFileManager defaultManager] removeItemAtPath:fileURL.path error:&error];
                                                                              if (isFileDeleted) {
                                                                                  NSLog(@"Deleted Existing File");
                                                                              }
                                                                          }
                                                                      }
                                                                      
                                                                      return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];}
                                                            completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                                                                if (completion) {
                                                                    completion();
                                                                }
                                                                NSLog(@"File downloaded to: %@", filePath);
                                                            }];
    [downloadTask resume];
}

- (void)updatePlayingProgress:(NSTimer *)timer {
    CGFloat duration = self.player.duration;
    CGFloat currentTime = self.player.currentTime;
    CGFloat progress = duration > 0 ? currentTime/duration : 0.f;
    
    self.progress = progress;
    
    NSLog(@"%f", progress);
}

- (void)resetPlayingCell {
    DTPresetTableViewCell *playingCell = [self.tableView cellForRowAtIndexPath:self.playingCellIndexPath];
    self.playingCellIndexPath = nil;
    playingCell.state = kDTPresetTableViewCellStateDefault;
    playingCell.progressTimePoint = 0;
}

- (void)stopPlayingAudio {
    [self.progressTimer invalidate];
    [self.player stop];
}

- (void)configureCell:(DTPresetTableViewCell *)cell withPreset:(DTPreset *)preset {
    cell.name = preset.name;
    cell.createdBy = preset.createdBy;
    cell.tags = [DTPreset preparedTagsStringFromString:preset.tags];
    cell.specification = preset.specification;
    cell.audioURL = [NSURL URLWithString:preset.audioPreview];
    cell.imageURL = [NSURL URLWithString:preset.coverImage];
    cell.hasAudio = [preset.audioPreview length] > 0;
}

- (NSArray *)presetConfigDictionariesFromFile:(NSString *)fileName; {
    NSURL *documentsURL = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *fileURL = [documentsURL URLByAppendingPathComponent:fileName];
    NSData *presetConfigData = nil;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:fileURL.path]) {
        presetConfigData = [[NSData alloc] initWithContentsOfURL:fileURL];

    } else {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
        presetConfigData = [[NSData alloc] initWithContentsOfFile:filePath];
    }
    
    NSError *error = nil;
    NSArray *presetConfigDictionaries = [NSJSONSerialization JSONObjectWithData:presetConfigData
                                                                        options:kNilOptions
                                                                          error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
    
    return presetConfigDictionaries;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.presets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DTPresetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kDTPresetViewControllerCellReuseIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[DTPresetTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kDTPresetViewControllerCellReuseIdentifier];
    }
    cell.delegate = self;
    DTPreset *preset = self.presets[indexPath.row];
    [self configureCell:cell withPreset:preset];
    cell.state = kDTPresetTableViewCellStateDefault;
    if (indexPath == self.playingCellIndexPath) {
        if (self.player) {
            if (self.player.isPlaying) {
                cell.state = kDTPresetTableViewCellStatePlayingAudio;
                cell.progressTimePoint = self.progress;
            }
        } else {
            cell.state = kDTPresetTableViewCellStateLoadingAudio;
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DTDetailViewController *detailViewController = [[DTDetailViewController alloc] init];
    DTPresetTableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if (cell.hasAudio) {
        cell.state = kDTPresetTableViewCellStateDefault;
    }
    [self resetPlayingCell];
    DTPreset *preset = self.presets[indexPath.row];
    detailViewController.preset = preset;
    detailViewController.imageURL = cell.imageURL;
    detailViewController.hasAudio = cell.hasAudio;
    detailViewController.audioURL = cell.audioURL;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - VKPresetTableViewCellDelegate

- (void)presetTableViewCell:(DTPresetTableViewCell *)presetTableViewCell didTouchUpInsidePlayButton:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:presetTableViewCell];
    if (self.player.isPlaying) {
        [self.player stop];
        self.progress = 0.f;
        if (self.progressTimer.isValid) {
            [self.progressTimer invalidate];
            self.progressTimer = nil;
        }
        DTPresetTableViewCell *playingCell = [self.tableView cellForRowAtIndexPath:self.playingCellIndexPath];
        if (playingCell.state == kDTPresetTableViewCellStatePlayingAudio) {
            playingCell.state = kDTPresetTableViewCellStateDefault;
        }
    } else if ([indexPath isEqual:self.playingCellIndexPath]) {
        [self.player play];
        self.progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.3
                                                                         target:self
                                                                       selector:@selector(updatePlayingProgress:)
                                                                       userInfo:nil
                                                                        repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.progressTimer forMode:NSRunLoopCommonModes];
        presetTableViewCell.state = kDTPresetTableViewCellStatePlayingAudio;
        
        return;
    }
    
    if (![indexPath isEqual:self.playingCellIndexPath]) {
        self.player = nil;
        self.playingCellIndexPath = indexPath;
        for (DTPresetTableViewCell *cell in self.tableView.visibleCells) {
            if (cell.state == kDTPresetTableViewCellStateLoadingAudio) {
                cell.state = kDTPresetTableViewCellStateDefault;
            }
        }
        presetTableViewCell.state = kDTPresetTableViewCellStateLoadingAudio;
        DTPreset *preset = self.presets[indexPath.row];
        NSURL *url = [NSURL URLWithString:preset.audioPreview];
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *audioData = [NSData dataWithContentsOfURL:url];
            dispatch_async(dispatch_get_main_queue(), ^{
                __strong typeof(self) strongSelf = weakSelf;
                DTPresetTableViewCell *playingCell = [strongSelf.tableView cellForRowAtIndexPath:strongSelf.playingCellIndexPath];
                NSError *error = nil;
                strongSelf.player = [[AVAudioPlayer alloc] initWithData:audioData fileTypeHint:AVFileTypeMPEGLayer3 error:&error];
                strongSelf.player.delegate = strongSelf;
                if (error) {
                    NSLog(@"Error creating player: %@", error);
                    [DTAlertAudioError presentPlayingAudioErrorAlertFromViewController:self];
                    playingCell.state = kDTPresetTableViewCellStateDefault;
                    
                    return;
                }
                
                [strongSelf.player play];
                strongSelf.progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.3
                                                                                 target:strongSelf
                                                                               selector:@selector(updatePlayingProgress:)
                                                                               userInfo:nil
                                                                                repeats:YES];
                [[NSRunLoop currentRunLoop] addTimer:strongSelf.progressTimer forMode:NSRunLoopCommonModes];
                playingCell.state = kDTPresetTableViewCellStatePlayingAudio;
            });
        });
    }
    
//    if (presetTableViewCell.state == kVKPresetTableViewCellStateDefault) {
//        presetTableViewCell.state = kVKPresetTableViewCellStateLoadingAudio;
//        presetTableViewCell.userInteractionEnabled = NO;
//
//        NSIndexPath *indexPath = [self.tableView indexPathForCell:presetTableViewCell];
//
//        DTPreset *preset = self.presets[indexPath.row];
//
//        __block VKPresetTableViewCell *presetTableViewCellBlock = presetTableViewCell;
//        self.playingCellIndexPath = indexPath;
//        NSURL *url = [NSURL URLWithString:preset.audioPreview];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            NSData *audioData = [NSData dataWithContentsOfURL:url];
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                NSError* error = nil;
//                self.player = [[AVAudioPlayer alloc] initWithData:audioData fileTypeHint:AVFileTypeMPEGLayer3 error:&error];
//                if(error) {
//                    NSLog(@"Error creating player: %@", error);
//                    [DTAlerts presentPlayingAudioErrorAlertFromViewController:self];
//                    presetTableViewCellBlock.state = kVKPresetTableViewCellStateDefault;
//                    presetTableViewCellBlock.userInteractionEnabled = YES;
//
//                    return;
//                }
//
//                self.player.delegate = self;
//
//                [self.player play];
//                presetTableViewCellBlock.state = kVKPresetTableViewCellStatePlayingAudio;
//                self.checkProgressTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(playbackProgress:) userInfo:nil repeats:YES];
//                presetTableViewCellBlock.userInteractionEnabled = YES;
////                self.playingCellIndexPath = indexPath;
//            });
//        });
//
//    }
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self resetPlayingCell];
}

@end
