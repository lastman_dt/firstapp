//
//  DTDetailViewController.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 16/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTPreset.h"

NS_ASSUME_NONNULL_BEGIN

@interface DTDetailViewController : UIViewController

@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *createdBy;
@property (nonatomic, strong) NSString *specification;
@property (nonatomic, strong) NSString *tags;
@property (nonatomic, assign) BOOL hasAudio;
@property (nonatomic, assign) NSURL *audioURL;
@property (nonatomic, strong) DTPreset *preset;

@end

NS_ASSUME_NONNULL_END
