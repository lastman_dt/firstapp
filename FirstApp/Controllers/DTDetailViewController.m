//
//  DTDetailViewController.m
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 16/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "DTDetailViewController.h"
#import "DTPresetView.h"
#import <AVFoundation/AVFoundation.h>
#import "DTAlertAudioError.h"
#import "UIFont+DTFont.h"

@interface DTDetailViewController () <DTPresetViewDelegate, AVAudioPlayerDelegate>

@property (nonatomic, strong) AVAudioPlayer* player;

@end

@implementation DTDetailViewController

- (void)loadView {
    DTPresetView *view = [[DTPresetView alloc] init];
    view.delegate = self;
    self.view = view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureViewController];
}

- (void)configureViewController {
    
    self.title = @"Preset Info";
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],
                                                                      NSFontAttributeName:[UIFont dt_extraSmallFont]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.backBarButtonItem.tintColor = [UIColor whiteColor];
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    [self.player stop];
}

- (void)setPreset:(DTPreset *)preset {
    _preset = preset;
    DTPresetView *presetView = (DTPresetView *)self.view;
    presetView.preset = _preset;
}

- (void)setImageURL:(NSURL *)imageURL {
    _imageURL = imageURL;
    DTPresetView *presetView = (DTPresetView *)self.view;
    presetView.imageURL = _imageURL;
}

- (void)setHasAudio:(BOOL)hasAudio {
    _hasAudio = hasAudio;
    DTPresetView *presetView = (DTPresetView *)self.view;
    presetView.hasAudio = _hasAudio;
}

- (void)setAudioURL:(NSURL *)audioURL {
    _audioURL = audioURL;
}

- (void)presetView:(DTPresetView *)presetView didTouchUpInsidePlayButton:(id)sender {
    
    if (presetView.state == kDTPresetViewStateDefault) {
        presetView.state =kDTPresetViewStateLoadingAudio;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSData *audioData = [NSData dataWithContentsOfURL:self.audioURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError* error = nil;
                self.player = [[AVAudioPlayer alloc] initWithData:audioData error:&error];
                
                if(error) {
                    NSLog(@"Error creating player: %@", error);
                    [DTAlertAudioError presentPlayingAudioErrorAlertFromViewController:self];
                    
                    return;
                }
                
                self.player.delegate = self;
                presetView.state = kDTPresetViewStatePlayingAudio;
                [self.player play];
            });
        });
    } else if (presetView.state == kDTPresetViewStatePlayingAudio) {
        presetView.state = kDTPresetViewStateDefault;
        [self.player stop];
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    
    DTPresetView *presetView = (DTPresetView *)self.view;
    presetView.state = kDTPresetViewStateDefault;
}

@end
