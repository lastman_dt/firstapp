//
//  DTPresentViewController.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 07/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DTPresetViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
