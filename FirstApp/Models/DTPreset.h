//
//  DTPreset.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 08/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "RLMObject.h"
#import <Realm/Realm.h>

NS_ASSUME_NONNULL_BEGIN

@interface DTPreset : RLMObject

@property NSInteger idItem;
@property NSString *name;
@property NSInteger orderBy;
@property NSString *audioPreview;
@property NSString *createdBy;
@property NSString *tags;
@property NSString *coverImage;
@property NSString *specification;

+ (NSString *)preparedTagsStringFromString:(NSString *)tags;

+ (NSString *)localizedSpecificationFromDictionary:(NSDictionary *)dictionary;

+ (DTPreset *)presetFromConfigDictionary:(NSDictionary *)presetConfigDictionary;

- (BOOL)isEqual:(id)object;

- (BOOL)isEqualToPreset:(DTPreset *)presetFromConfig;

@end

NS_ASSUME_NONNULL_END
