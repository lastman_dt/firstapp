//
//  DTPreset.m
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 08/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "DTPreset.h"

@implementation DTPreset

+ (NSString *)primaryKey {
    
    return @"idItem";
}

+ (NSString *)preparedTagsStringFromString:(NSString *)tags {
    
    NSArray *words = [tags componentsSeparatedByString:@";"];
    NSString *result = @"";
    for (int i = 0; i < words.count; i++) {
        result = [result stringByAppendingFormat:@"#%@ ", words[i]];
    }

    return result;
}

+ (NSString *)localizedSpecificationFromDictionary:(NSDictionary *)dictionary {
    NSString *language = [[NSLocale preferredLanguages] firstObject];
    NSString *localizedSpecification = @"";
    for (NSString *key in dictionary) {
        if ([language containsString:key]) {
            localizedSpecification = [dictionary valueForKey:key];
            
            break;
        }
    }
    
    return localizedSpecification;
}

+ (DTPreset *)presetFromConfigDictionary:(NSDictionary *)presetConfigDictionary {
    DTPreset *preset = [[DTPreset alloc] init];
    preset.idItem = [presetConfigDictionary[@"id"] integerValue];
    preset.name = presetConfigDictionary[@"name"];
    preset.orderBy = [presetConfigDictionary[@"orderBy"] integerValue];
    preset.audioPreview = presetConfigDictionary[@"audioPreview"];
    preset.createdBy = presetConfigDictionary[@"createdBy"];
    preset.tags = presetConfigDictionary[@"tags"];
    preset.coverImage = presetConfigDictionary[@"coverImage"];
    preset.specification = [DTPreset localizedSpecificationFromDictionary:presetConfigDictionary[@"description"]];
    
    return preset;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[DTPreset class]]) {
        return NO;
    }
    
    return [self isEqualToPreset:(DTPreset *)object];
}

- (BOOL)isEqualToPreset:(DTPreset *)preset {
    return self.idItem  == preset.idItem
            && ([self.name isEqual:preset.name] || (!self.name && !preset.name))
            && ([self.createdBy isEqual:preset.createdBy] || (!self.createdBy && !preset.createdBy))
            && ([self.specification isEqual:preset.specification] || (!self.specification && !preset.specification))
            && ([self.audioPreview isEqual:preset.audioPreview]|| (!self.audioPreview && !preset.audioPreview))
            && ([self.tags isEqual:preset.tags]|| (!self.tags && !preset.tags))
            && ([self.coverImage isEqual:preset.coverImage] || (!self.coverImage && !preset.coverImage))
            && self.orderBy == preset.orderBy;
}
    
@end
