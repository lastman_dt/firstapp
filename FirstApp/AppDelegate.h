//
//  AppDelegate.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 07/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

