//
//  DTPresetView.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 17/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTPreset.h"

typedef NS_ENUM(NSInteger, DTPresetViewState) {
    kDTPresetViewStateDefault,
    kDTPresetViewStateLoadingAudio,
    kDTPresetViewStatePlayingAudio
};

NS_ASSUME_NONNULL_BEGIN

@protocol DTPresetViewDelegate;

@interface DTPresetView : UIView

@property (nonatomic, strong) id <DTPresetViewDelegate> delegate;
@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *createdBy;
@property (nonatomic, strong) NSString *specification;
@property (nonatomic, strong) NSString *tags;
@property (nonatomic, assign) BOOL hasAudio;
@property (nonatomic, strong) NSURL *audioURL;
@property (nonatomic, strong) DTPreset *preset;
@property (nonatomic, assign) DTPresetViewState state;

@end

@protocol DTPresetViewDelegate <NSObject>

- (void)presetView:(DTPresetView *)presetView didTouchUpInsidePlayButton:(id)sender;

@end


NS_ASSUME_NONNULL_END
