//
//  VKPresetTableViewCell.m
//  FirstApp
//
//  Created by Vasiliy Korchagin on 13/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "DTPresetTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+AFNetworking.h"
#import "UIFont+DTFont.h"
#import "UIColor+DTColor.h"

CGFloat const kDTPresetTableViewCellInset = 8.f;
CGFloat const kLDPPresetCellCreatedByLabelAlpha = 0.7f;
CGFloat const kLDPPresetCellTagsLabelAlpha = 0.5f;
CGFloat const kPresetCellImageViewCornerRadius = 4.f;

NSString * const kDTPresetCellPlayButtonImage = @"playIcon";
NSString * const kDTPresetCellPauseButtonImage = @"pauseIcon";

@interface DTPresetTableViewCell()

@property (nonatomic, strong) UIImageView *presetImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *createdByLabel;
@property (nonatomic, strong) UILabel *tagsLabel;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) UIProgressView *progressBar;

@end

@implementation DTPresetTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self configureView];
        [self createSubviews];
    }
    
    return self;
}

#pragma mark - Custom Accessors

- (void)setName:(NSString *)name {
    _name = name;
    self.nameLabel.text = _name;
    [self.nameLabel sizeToFit];
}

- (void)setCreatedBy:(NSString *)createdBy {
    _createdBy = createdBy;
    self.createdByLabel.text = _createdBy;
    [self.createdByLabel sizeToFit];
}

- (void)setImageURL:(NSURL *)imageURL {
    _imageURL = imageURL;
    [self.presetImageView setImageWithURL:_imageURL placeholderImage:[[UIImage alloc] init]];
}

- (void)setTags:(NSString *)tags {
    _tags = tags;
    self.tagsLabel.text = _tags;
    [self.tagsLabel sizeToFit];
}

- (void)setHasAudio:(BOOL)hasAudio {
    _hasAudio = hasAudio;

    [self definePlayerButtonVisibility];
    [self defineProgressBarVisibility];
}

- (void)setProgressTimePoint:(CGFloat)progressTimePoint {
    _progressTimePoint = progressTimePoint;
    [self.progressBar setProgress:_progressTimePoint animated:YES];
}

- (void)setState:(DTPresetTableViewCellState)state {
    _state = state;
    switch (_state) {
        case kDTPresetTableViewCellStateDefault:
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
            [self.playButton setImage:[UIImage imageNamed:kDTPresetCellPlayButtonImage] forState:UIControlStateNormal];
            self.progressTimePoint = 0.f;
            self.userInteractionEnabled = YES;
            break;
        case kDTPresetTableViewCellStateLoadingAudio:
            self.activityIndicator.hidden = NO;
            [self.activityIndicator startAnimating];
            [self.playButton setImage:nil forState:UIControlStateNormal];
            self.progressTimePoint = 0.f;
            self.userInteractionEnabled = NO;
            break;
        case kDTPresetTableViewCellStatePlayingAudio:
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
            [self.playButton setImage:[UIImage imageNamed:kDTPresetCellPauseButtonImage] forState:UIControlStateNormal];
            self.userInteractionEnabled = YES;
            break;
       default:
            break;
    }
    [self definePlayerButtonVisibility];
    [self defineProgressBarVisibility];
}

#pragma mark - Actions

- (IBAction)didTouchUpInsidePlayButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(presetTableViewCell:didTouchUpInsidePlayButton:)]) {
        [self.delegate presetTableViewCell:self didTouchUpInsidePlayButton:sender];
    }
}

#pragma mark - Configure View

- (void)configureView {
    self.backgroundColor = [UIColor dt_darkDesaturatedBlueColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)definePlayerButtonVisibility {
    self.playButton.hidden = !self.hasAudio || (self.state == kDTPresetTableViewCellStateLoadingAudio);
}

- (void)defineProgressBarVisibility {
    self.progressBar.hidden = !self.hasAudio || (self.state == kDTPresetTableViewCellStateLoadingAudio || self.state == kDTPresetTableViewCellStateDefault);
}

#pragma mark - Creating Subviews

- (void)createSubviews {
    [self createPresetImageView];
    [self createNameLabel];
    [self createCreatedByLabel];
    [self createTagsLabel];
    [self createPlayButton];
    [self createActivityIndicator];
    [self createProgressBar];
}

- (void)createPresetImageView {
    self.presetImageView = [[UIImageView alloc] init];
    self.presetImageView.backgroundColor = [UIColor dt_darkBlueColor];
    self.presetImageView.layer.cornerRadius = kPresetCellImageViewCornerRadius;
    self.presetImageView.clipsToBounds = YES;
    [self addSubview:self.presetImageView];
}

- (void)createNameLabel {
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont dt_bigFont];
    self.nameLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.nameLabel];
}

- (void)createCreatedByLabel {
    self.createdByLabel = [[UILabel alloc] init];
    self.createdByLabel.font = [UIFont dt_smallFont];
    self.createdByLabel.textColor = [[UIColor whiteColor]
                                     colorWithAlphaComponent:kLDPPresetCellCreatedByLabelAlpha];
    [self addSubview:self.createdByLabel];
}

- (void)createTagsLabel {
    self.tagsLabel = [[UILabel alloc] init];
    self.tagsLabel.font = [UIFont dt_extraSmallFont];
    self.tagsLabel.textColor = [[UIColor whiteColor]
                                colorWithAlphaComponent:kLDPPresetCellTagsLabelAlpha];
    [self addSubview:self.tagsLabel];
}

- (void)createPlayButton {
    self.playButton = [[UIButton alloc] init];
    [self.playButton setImage:[UIImage imageNamed:kDTPresetCellPlayButtonImage]
                     forState:UIControlStateNormal];
    self.playButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.playButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
    self.playButton.contentHorizontalAlignment = UIControlContentVerticalAlignmentFill;
    [self.playButton addTarget:self
                        action:@selector(didTouchUpInsidePlayButton:)
              forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.playButton];
}

- (void)createActivityIndicator {
    self.activityIndicator = [[UIActivityIndicatorView alloc]
                              initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self addSubview:self.activityIndicator];
}

- (void)createProgressBar {
    self.progressBar = [[UIProgressView alloc] init];
    self.progressBar.tintColor = [UIColor dt_pureYellowColor];
    self.progressBar.trackTintColor = [UIColor dt_darkDesaturatedBlueColor];
    [self addSubview:self.progressBar];
}

#pragma mark - Layout Subviews

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self layoutPresetImageView];
    [self layoutNameLabel];
    [self layoutCreatedByLabel];
    [self layoutTagsLabel];
    [self layoutPlayButton];
    [self layoutActivityIndicator];
    [self layoutProgressBar];
}

- (void)layoutPresetImageView {
    CGRect frame = self.presetImageView.frame;
    frame.origin.x = kDTPresetTableViewCellInset;
    frame.origin.y = kDTPresetTableViewCellInset;
    frame.size.height = CGRectGetHeight(self.frame) - 4 * kDTPresetTableViewCellInset;
    frame.size.width = CGRectGetHeight(self.frame) - 4 * kDTPresetTableViewCellInset;
    self.presetImageView.frame = frame;
}

- (void)layoutNameLabel {
    CGRect frame = self.nameLabel.frame;
    frame.origin.x = CGRectGetMaxX(self.presetImageView.frame) + 2 * kDTPresetTableViewCellInset;
    frame.origin.y = kDTPresetTableViewCellInset;
    self.nameLabel.frame = frame;
}

- (void)layoutCreatedByLabel {
    CGRect frame = self.createdByLabel.frame;
    frame.origin.x = CGRectGetMaxX(self.presetImageView.frame) + 2 * kDTPresetTableViewCellInset;
    frame.origin.y = CGRectGetMaxY(self.nameLabel.frame) + kDTPresetTableViewCellInset;
    self.createdByLabel.frame = frame;
}

- (void)layoutTagsLabel {
    CGRect frame = self.tagsLabel.frame;
    frame.origin.x = CGRectGetMaxX(self.presetImageView.frame) + 2 * kDTPresetTableViewCellInset;
    frame.origin.y = CGRectGetMaxY(self.createdByLabel.frame) + 2 * kDTPresetTableViewCellInset;
    self.tagsLabel.frame = frame;
}

- (void)layoutPlayButton {
    CGRect frame = self.playButton.frame;
    frame.size.width = CGRectGetHeight(self.frame) / 3;
    frame.size.height = CGRectGetWidth(frame);
    frame.origin.y = (CGRectGetHeight(self.frame) - CGRectGetHeight(frame)) / 2;
    frame.origin.x = CGRectGetWidth(self.frame) - CGRectGetWidth(frame) - 2 * kDTPresetTableViewCellInset;
    self.playButton.frame = frame;
}

- (void)layoutActivityIndicator {
    CGRect frame = self.playButton.frame;
    self.activityIndicator.frame = frame;
}

- (void)layoutProgressBar {
    CGRect frame = self.progressBar.frame;
    frame.origin.x = CGRectGetMinX(self.contentView.frame);
    frame.origin.y = CGRectGetMaxY(self.contentView.frame) - 2;
    frame.size.height = 2;
    frame.size.width = CGRectGetWidth(self.contentView.frame);
    self.progressBar.frame = frame;
}

@end
