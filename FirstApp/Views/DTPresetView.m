//
//  DTPresetView.m
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 17/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "DTPresetView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+AFNetworking.h"
#import "UIFont+DTFont.h"
#import "UIColor+DTColor.h"

CGFloat const kDTPresetViewLeftIndent = 32.f;
CGFloat const kDTPresetViewDownIndent = 8.f;
CGFloat const kDTPresetViewBottomIndent = 50.f;
CGFloat const kDTPresetViewTopIndent = 10.f;

NSString * const kDTPresetViewPlayButtonImage = @"baseline_play_circle_outline_white_36pt_1x.png";
NSString * const kDTPresetViewPauseButtonImage = @"baseline_pause_circle_outline_white_36pt_1x.png";

CGFloat const kLDPPresetViewSpecificationTextViewAlpha = 0.9f;
CGFloat const kLDPPresetViewCreatedByLabelAlpha = 0.7f;
CGFloat const kLDPPresetViewTagsLabelAlpha = 0.5f;

CGFloat const kPresetViewImageViewCornerRadius = 4.f;
CGFloat const kPresetViewImageViewSize = 200.f;

@interface DTPresetView()

@property (nonatomic, strong) UIImageView *presetImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *createdByLabel;
@property (nonatomic, strong) UILabel *tagsLabel;
@property (nonatomic, strong) UITextView *specificationTextView;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation DTPresetView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configureView];
        [self creatingSubview];
    }
    return self;
}

#pragma mark - Custom Accessors

- (void)setPreset:(DTPreset *)preset {
    _preset = preset;
    self.name = _preset.name;
    self.createdBy = _preset.createdBy;
    self.specification = _preset.specification;
    self.tags = _preset.tags;
}

- (void)setImageURL:(NSURL *)imageURL {
    _imageURL = imageURL;
    [self.presetImageView setImageWithURL:_imageURL];
}

- (void)setName:(NSString *)name {
    _name = name;
    self.nameLabel.text = _preset.name;
    [self.nameLabel sizeToFit];
}

- (void)setCreatedBy:(NSString *)createdBy {
    _createdBy = createdBy;
    self.createdByLabel.text = _createdBy;
    [self.createdByLabel sizeToFit];
}

- (void)setSpecification:(NSString *)specification {
    _specification = specification;
    self.specificationTextView.text = _specification;
}

- (void)setTags:(NSString *)tags {
    _tags = tags;
    self.tagsLabel.text = [DTPreset preparedTagsStringFromString:_tags];
    [self.tagsLabel sizeToFit];
}

- (void)setHasAudio:(BOOL)hasAudio {
    _hasAudio = hasAudio;
    if (!_hasAudio) {
        self.playButton.hidden = YES;
    } else {
        self.playButton.hidden = !_hasAudio;
    }
}

- (void)setState:(DTPresetViewState)state {
    _state = state;
    switch (_state) {
        case kDTPresetViewStateDefault:
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
            self.playButton.hidden = NO;
            [self.playButton setImage:[UIImage imageNamed:kDTPresetViewPlayButtonImage] forState:UIControlStateNormal];
            break;
        case kDTPresetViewStateLoadingAudio:
            self.activityIndicator.hidden = NO;
            [self.activityIndicator startAnimating];
            self.playButton.hidden = YES;
            [self.playButton setImage:nil forState:UIControlStateNormal];
            break;
        case kDTPresetViewStatePlayingAudio:
            self.activityIndicator.hidden = YES;
            [self.activityIndicator stopAnimating];
            self.playButton.hidden = NO;
            [self.playButton setImage:[UIImage imageNamed:kDTPresetViewPauseButtonImage] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

#pragma mark - Actions

- (IBAction)didTouchUpInsidePlayButton:(id)sender {
    if ([self.delegate respondsToSelector:@selector(presetView:didTouchUpInsidePlayButton:)]) {
        [self.delegate presetView:self didTouchUpInsidePlayButton:sender];
    }
}

#pragma mark - Configure View

- (void)configureView {
    self.backgroundColor = [UIColor dt_darkDesaturatedBlueColor];
}

#pragma - Creating Subviews

- (void)creatingSubview {
    [self createPresetImageView];
    [self createNameLabel];
    [self createCreatedByLabel];
    [self createSpecificationTextView];
    [self createTagsLabel];
    [self createPlayButton];
    [self createActivityIndicator];
}

- (void)createPresetImageView {
    self.presetImageView = [[UIImageView alloc] init];
    self.presetImageView.backgroundColor = [UIColor dt_darkBlueColor];
    self.presetImageView.layer.cornerRadius = kPresetViewImageViewCornerRadius;
    self.presetImageView.clipsToBounds = YES;
    [self addSubview:self.presetImageView];
}

- (void)createNameLabel {
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.textColor = [UIColor whiteColor];
    self.nameLabel.font = [UIFont dt_largeBoldFont];
    [self addSubview:self.nameLabel];
}

- (void)createCreatedByLabel {
    self.createdByLabel = [[UILabel alloc] init];
    self.createdByLabel.font = [UIFont dt_smallFont];
    self.createdByLabel.textColor = [[UIColor whiteColor]
                                     colorWithAlphaComponent:kLDPPresetViewCreatedByLabelAlpha];
    [self addSubview:self.createdByLabel];
}

- (void)createSpecificationTextView {
    self.specificationTextView = [[UITextView alloc] init];
    self.specificationTextView.backgroundColor = [UIColor dt_darkDesaturatedBlueColor];
    [self.specificationTextView setFont:[UIFont dt_mediumFont]];
    [self.specificationTextView setTextColor:[UIColor whiteColor]];
    self.specificationTextView.alpha = kLDPPresetViewSpecificationTextViewAlpha;
    self.specificationTextView.editable = NO;
    [self addSubview:self.specificationTextView];
}

- (void)createTagsLabel {
    self.tagsLabel = [[UILabel alloc] init];
    self.tagsLabel.font = [UIFont dt_extraSmallFont];
    self.tagsLabel.textColor = [[UIColor whiteColor] colorWithAlphaComponent:kLDPPresetViewTagsLabelAlpha];
    [self addSubview:self.tagsLabel];
}

- (void)createPlayButton {
    self.playButton = [[UIButton alloc] init];
    [self.playButton setImage:[UIImage imageNamed:kDTPresetViewPlayButtonImage]
                     forState:UIControlStateNormal];
    [self.playButton addTarget:self
                        action:@selector(didTouchUpInsidePlayButton:)
              forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.playButton];
}

- (void)createActivityIndicator {
    self.activityIndicator = [[UIActivityIndicatorView alloc]
                              initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self addSubview:self.activityIndicator];
}

#pragma mark - Layout Subviews

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self layoutPresetImageView];
    [self layoutNameLabel];
    [self layoutCreatedByLabel];
    [self layoutTagsLabel];
    [self layoutSpecificationTextView];
    [self layoutPlayButton];
    [self layoutActivityIndicator];
}

- (void)layoutPresetImageView {
    CGRect frame = self.presetImageView.frame;
    frame.size.height = kPresetViewImageViewSize;
    frame.size.width = kPresetViewImageViewSize;
    frame.origin.x = CGRectGetWidth(self.frame) / 2 - CGRectGetWidth(frame) / 2;
    frame.origin.y = kDTPresetViewTopIndent;
    self.presetImageView.frame = frame;
}

- (void)layoutNameLabel {
    CGRect frame = self.nameLabel.frame;
    frame.origin.x = kDTPresetViewLeftIndent;
    frame.origin.y = CGRectGetMaxY(self.presetImageView.frame) + kDTPresetViewDownIndent;
    self.nameLabel.frame = frame;
}

- (void)layoutCreatedByLabel {
    CGRect frame = self.createdByLabel.frame;
    frame.origin.x = kDTPresetViewLeftIndent;
    frame.origin.y = CGRectGetMaxY(self.nameLabel.frame) + kDTPresetViewDownIndent / 2;
    self.createdByLabel.frame = frame;
}

- (void)layoutTagsLabel {
    CGRect frame = self.tagsLabel.frame;
    frame.origin.x = kDTPresetViewLeftIndent;
    frame.origin.y = CGRectGetMaxY(self.frame) - kDTPresetViewBottomIndent;
    self.tagsLabel.frame = frame;
}

- (void)layoutSpecificationTextView {
    CGRect frame = self.specificationTextView.frame;
    frame.origin.x = kDTPresetViewLeftIndent;
    frame.origin.y = CGRectGetMaxY(self.createdByLabel.frame) + kDTPresetViewDownIndent;
    frame.size.width = CGRectGetWidth(self.frame) - kDTPresetViewLeftIndent * 2;
    frame.size.height = CGRectGetMinY(self.tagsLabel.frame) - CGRectGetMaxY(self.createdByLabel.frame) - kDTPresetViewDownIndent * 2;
    self.specificationTextView.frame = frame;
}

- (void)layoutPlayButton {
    [self.playButton sizeToFit];
    CGRect frame = self.playButton.frame;
    frame.origin.x = CGRectGetMaxX(self.presetImageView.frame) + kDTPresetViewLeftIndent;
    frame.origin.y = CGRectGetMinY(self.nameLabel.frame);
    self.playButton.frame = frame;
}

- (void)layoutActivityIndicator {
    self.activityIndicator.frame = self.playButton.frame;;
}

@end
