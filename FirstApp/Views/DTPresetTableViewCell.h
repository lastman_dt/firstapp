//
//  VKPresetTableViewCell.h
//  FirstApp
//
//  Created by Vasiliy Korchagin on 13/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DTPresetTableViewCellState) {
    kDTPresetTableViewCellStateDefault,
    kDTPresetTableViewCellStateLoadingAudio,
    kDTPresetTableViewCellStatePlayingAudio
};

NS_ASSUME_NONNULL_BEGIN

@protocol DTPresetTableViewCellDelegate;

@interface DTPresetTableViewCell : UITableViewCell

@property (nonatomic, weak) id <DTPresetTableViewCellDelegate> delegate;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *createdBy;
@property (nonatomic, strong) NSString *tags;
@property (nonatomic, strong) NSString *specification;
@property (nonatomic, assign) BOOL hasAudio;
@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSURL *audioURL;
@property (nonatomic, assign) DTPresetTableViewCellState state;
@property (nonatomic) CGFloat progressTimePoint;

@end

@protocol DTPresetTableViewCellDelegate <NSObject>

- (void)presetTableViewCell:(DTPresetTableViewCell *)presetTableViewCell didTouchUpInsidePlayButton:(id)sender;

@end

NS_ASSUME_NONNULL_END
