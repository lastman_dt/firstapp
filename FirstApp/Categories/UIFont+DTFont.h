//
//  UIFont+DTFont.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 11/07/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (DTFont)

+ (UIFont *)dt_largeBoldFont;

+ (UIFont *)dt_bigFont;

+ (UIFont *)dt_mediumFont;

+ (UIFont *)dt_smallFont;

+ (UIFont *)dt_extraSmallFont;

@end

NS_ASSUME_NONNULL_END
