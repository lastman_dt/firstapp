//
//  UIColor+DTPreset.m
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 16/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "UIColor+DTPreset.h"

@implementation UIColor (DTPreset)

+ (UIColor *)dt_darkDesaturatedBlueColor {
    
    UIColor *dt_darkDesaturatedBlueColor = [UIColor colorWithRed:34.f/255.f
                                                           green:41.f/255.f
                                                            blue:58.f/255.f
                                                           alpha:1.f];
    return dt_darkDesaturatedBlueColor;
}

+ (UIColor *)dt_veryDarkBlueColor {
    
    UIColor *dt_veryDarkBlueColor = [UIColor colorWithRed:30.f/255.f
                                                    green:36.f/255.f
                                                     blue:50.f/255.f
                                                    alpha:1.f];
    return dt_veryDarkBlueColor;
}

+ (UIColor *)dt_darkBlueColor {
    
    UIColor *dt_darkBlueColor = [UIColor colorWithRed:53.f/255.f
                                             green:63.f/255.f
                                              blue:87.f/255.f
                                             alpha:1.f];
    
    return dt_darkBlueColor;
}

+ (UIColor *)dt_pureYellowColor {
    
    UIColor *dt_pureYellowColor = [UIColor colorWithRed:232.f/255.f
                                                  green:227.f/255.f
                                                   blue:0
                                                  alpha:1.f];
    return dt_pureYellowColor;
}

@end
