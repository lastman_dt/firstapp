//
//  UIFont+DTPreset.m
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 23/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import "UIFont+DTPreset.h"

@implementation UIFont (DTPreset)

+ (UIFont *)dt_largeBoldFont {
   return [UIFont boldSystemFontOfSize:32.f];
}

+ (UIFont *)dt_bigFont {
    return [UIFont systemFontOfSize:18.f];
}

+ (UIFont *)dt_mediumFont {
    return [UIFont systemFontOfSize:16.f];
}

+ (UIFont *)dt_smallFont {
    return [UIFont systemFontOfSize:14.f];
}

+ (UIFont *)dt_extraSmallFont {
    return [UIFont systemFontOfSize:12.f];
}

@end
