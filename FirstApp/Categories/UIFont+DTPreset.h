//
//  UIFont+DTPreset.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 23/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (DTPreset)

+ (UIFont *)dt_largeBoldFont;

+ (UIFont *)dt_bigFont;

+ (UIFont *)dt_mediumFont;

+ (UIFont *)dt_smallFont;

+ (UIFont *)dt_extraSmallFont;

@end

NS_ASSUME_NONNULL_END
