//
//  UIColor+DTPreset.h
//  FirstApp
//
//  Created by Дмитрий Трошкин  on 16/05/2019.
//  Copyright © 2019 Dmitry Troshkin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (DTPreset)

+ (UIColor *)dt_darkDesaturatedBlueColor;

+ (UIColor *)dt_veryDarkBlueColor;

+ (UIColor *)dt_darkBlueColor;

+ (UIColor *)dt_pureYellowColor;

@end

NS_ASSUME_NONNULL_END
